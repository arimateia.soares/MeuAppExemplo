import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TelaHome from '../telas/TelaHome';
import TelaSettings from '../telas/TelaSettings';

const Tab = createBottomTabNavigator();

function NavegadorPrincipal() {
    return (
        <Tab.Navigator>
            <Tab.Screen name="Home" component={TelaHome} />
            <Tab.Screen name="Settings" component={TelaSettings} />
        </Tab.Navigator>
    )
}

export default NavegadorPrincipal;